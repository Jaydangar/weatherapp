package android.example.com.weather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //the recyclerview
    RecyclerView recyclerView;

    //  List of Cities
    List<ModelClass> CityList;

    private String URL = "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=e3595bf348aa3c048413af7d05fb868a&q=";
    private String[] CityNames = {"Bangalore","Delhi","Mumbai"};

    private String TAG = this.getClass().getName();

    private ArrayList<String> URLs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        URLs = new ArrayList<>();

        for (String Cities: CityNames) {
            URLs.add(URL+Cities);
        }

        //getting the recyclerview from xml
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initializing the Citylist
        CityList = new ArrayList<>();

        for (String CityURLs:URLs) {
            AddIntoList(CityURLs);
        }
    }

    private void AddIntoList(String URL){
        // Instantiate the RequestQueue.
        final RequestQueue queue = Volley.newRequestQueue(this);
        Log.d(TAG, "onCreate: " + URL);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject root = new JSONObject(response);
                            String CityName = root.getJSONObject("city").getString("name");
                            JSONArray jsonList = root.getJSONArray("list");

                            //  Getting First JSONObject Only...
                            JSONObject jsonObject = jsonList.getJSONObject(0);
                            JSONObject main = jsonObject.getJSONObject("main");
                            String Temp = main.getString("temp");
                            String TempMin = main.getString("temp_min");
                            String TempMax = main.getString("temp_max");
                            String Humidity = main.getString("humidity");

                            Log.d(TAG, "CityName: " + CityName);
                            Log.d(TAG, "Temp: " + Temp);
                            Log.d(TAG, "TempMin: " + TempMin);
                            Log.d(TAG, "TempMax: " + TempMax);
                            Log.d(TAG, "Humidity: " + Humidity);

                            CityList.add(new ModelClass(CityName,Temp,TempMin,TempMax,Humidity));

                            //creating recyclerview adapter
                            Adapter adapter = new Adapter(getApplicationContext(), CityList);

                            //setting adapter to recyclerview
                            recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.toString());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
