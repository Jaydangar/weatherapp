package android.example.com.weather;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.CityViewHolder> {

    //this context we will use to inflate the layout
    private Context mCtx;

    private List<ModelClass> CityDetailsList;

    //getting the context and product list with constructor
    public Adapter(Context mCtx, List<ModelClass> CityDetailsList) {
        this.mCtx = mCtx;
        this.CityDetailsList = CityDetailsList;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.custom, null);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        //getting the product of the specified position
        ModelClass modelClass = CityDetailsList.get(position);

        //binding the data with the viewholder views
        holder.CityName.setText("City Name is : " + modelClass.getCityName());
        holder.CityTemp.setText("City Temperature is : " + modelClass.getCityTemp() + "Kelvin");
        holder.CityMinTemp.setText("City Min Temperature is : " + modelClass.getCityMinTemp() + "Kelvin");
        holder.CityMaxTemp.setText("City Max Temperature is : " + modelClass.getCityMaxTemp() + "Kelvin");
        holder.CityHumidity.setText("City Humidity is : " + modelClass.getCityHumidity());
    }


    @Override
    public int getItemCount() {
        return CityDetailsList.size();
    }

    class CityViewHolder extends RecyclerView.ViewHolder {

        TextView CityName,CityTemp,CityMinTemp,CityMaxTemp,CityHumidity;

        public CityViewHolder(View view) {
            super(view);

            CityName = view.findViewById(R.id.CityName);
            CityTemp = view.findViewById(R.id.Temp);
            CityMaxTemp = view.findViewById(R.id.TempMax);
            CityMinTemp = view.findViewById(R.id.TempMin);
            CityHumidity = view.findViewById(R.id.Humidity);
        }
    }
}
