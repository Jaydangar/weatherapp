package android.example.com.weather;

import android.view.Display;

public class ModelClass {

    private String CityName;
    private String CityTemp;
    private String CityMinTemp;
    private String CityMaxTemp;
    private String CityHumidity;

    ModelClass(String CityName,String CityTemp,String CityMinTemp,String CityMaxTemp,String CityHumidity){
        this.CityName = CityName;
        this.CityTemp = CityTemp;
        this.CityMinTemp = CityMinTemp;
        this.CityMaxTemp = CityMaxTemp;
        this.CityHumidity = CityHumidity;
    }

    public String getCityName() {
        return CityName;
    }

    public String getCityTemp() {
        return CityTemp;
    }

    public String getCityMinTemp() {
        return CityMinTemp;
    }

    public String getCityMaxTemp() {
        return CityMaxTemp;
    }

    public String getCityHumidity() {
        return CityHumidity;
    }

}
